FROM openjdk:8-jre-buster

LABEL maintainer "Torpus"

ENV VERSION='.latest.release'

RUN apt-get update && apt-get install -y \
    openssl \
    jq \
    python python-dev python-pip \
    && rm -rf /var/cache/apk/*

RUN pip install mcstatus

HEALTHCHECK CMD mcstatus localhost ping

RUN addgroup --gid 1000 minecraft \
    && adduser --uid 1000 --gid 1000 --home /home/minecraft minecraft \
    && mkdir -m 777 /data /config /plugins \
    && chown minecraft:minecraft /data /config /plugins /home/minecraft
EXPOSE 25565

VOLUME ["/data","/config","/plugins"]

COPY start.sh /start.sh

RUN chmod +x /start.sh

WORKDIR /data

ENTRYPOINT [ "/start.sh" ]