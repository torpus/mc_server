# Minecraft Server in docker

This project will pull the latest release version of Minecraft Server and start it with the default configuration


## Create image and run in container

```bash
sudo docker build --rm -t mc .
sudo docker run -d -p 25565:25565 --name mc mc:latest
```
