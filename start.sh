#!/bin/bash
# should already be in /data via dockerfile WORKDIR
if [ ! -e /data/eula.txt ]; then
    echo "eula=TRUE" >> /data/eula.txt
fi

LATEST='.latest.release'
VERSION=$VERSION

if [[ "$VERSION" = "$LATEST" ]]; then
    VERSION_JSON=https://launchermeta.mojang.com/mc/game/version_manifest.json
    VANILLA_VERSION=$(curl -s $VERSION_JSON | jq -r $VERSION)
else
    VANILLA_VERSION='1.12.2'
fi
echo "$VANILLA_VERSION"

cd /data
MC_FILE="minecraft_server.$VANILLA_VERSION.jar"

if [ ! -e "$MC_FILE" ]; then
    echo "Downloading $MC_FILE..."
    wget -q -O "$MC_FILE" "$(curl -s "$(curl -s https://launchermeta.mojang.com/mc/game/version_manifest.json \
    | jq --arg VANILLA_VERSION "$VANILLA_VERSION" --raw-output '[.versions[]|select(.id == $VANILLA_VERSION)][0].url')" \
    | jq --raw-output '.downloads.server.url')"
fi

java -Xmx1024M -Xms1024M -jar "$MC_FILE" nogui